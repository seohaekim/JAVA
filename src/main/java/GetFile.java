import java.io.*;

public class GetFile {
    public static void main(String[] args) {
        FileInputStream file = null;
        BufferedReader br = null;

        ClassLoader loader = GetFile.class.getClassLoader();
        try {
            file = new FileInputStream(loader.getResource("F1014/file.txt").getFile());
            br  =  new BufferedReader(new InputStreamReader(file,"euc-kr"));

            String line = null;

            while((line = br.readLine()) != null){
                System.out.println(line);
            }
        } catch (FileNotFoundException e) {

        } catch (IOException e) {

        } finally {
            try{file.close(); br.close();} catch (IOException e){};
        }

    }
}
